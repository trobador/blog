{-# LANGUAGE OverloadedStrings #-}
import Hakyll
import Text.Pandoc
import Text.Pandoc.Walk
import Text.Pandoc.Definition

main :: IO ()
main = hakyll $ do
    match "images/*" $ do
        route   idRoute
        compile copyFileCompiler

    match "css/*.css" $ do
        route   idRoute
        compile compressCssCompiler

    match "fonts/*" $ do
        route   idRoute
        compile copyFileCompiler

    match "robots.txt" $ do
        route   idRoute
        compile copyFileCompiler

    match "posts/*.md" $ do
        route $ setExtension "html"
        compile $ pandocCompilerWithTransform defaultHakyllReaderOptions myHakyllWriterOptions relativeImagePath
            >>= saveSnapshot "content-body"
            >>= loadAndApplyTemplate "templates/post.html" postCtx
            >>= saveSnapshot "content-post"
            >>= loadAndApplyTemplate "templates/default.html" postCtx
            >>= relativizeUrls

    match "templates/*.html" $ compile templateBodyCompiler

    match "index.html" $ do
        route idRoute
        compile $ loadAllSnapshots "posts/*.md" "content-post"
            >>= fmap (take 3) . recentFirst
            >>= postsCompiler

    match "archive.html" $ do
        route idRoute
        compile $ loadAll "posts/*.md"
            >>= recentFirst
            >>= postsCompiler

    match "contact.html" $ do
        route idRoute
        compile $ getResourceBody
            >>= loadAndApplyTemplate "templates/default.html" defaultContext
            >>= relativizeUrls

    create ["rss.xml"] $ do
        route idRoute
        compile $ do
            let feedCtx = postCtx <> bodyField "description"
            posts <- fmap (take 10) . recentFirst =<<
                loadAllSnapshots "posts/*.md" "content-body"
            renderRss myFeedConfiguration feedCtx posts


relativeImagePath :: Pandoc -> Pandoc
relativeImagePath = walk transform where
    transform :: Inline -> Inline
    transform (Image attr inlines (url, title)) = Image attr inlines ( "/" <> url, title)
    transform x = x


postCtx :: Context String
postCtx =
    dateField "date" "%B %e, %Y" <>
    defaultContext


postsCompiler :: [Item String] -> Compiler (Item String)
postsCompiler posts = do
    let postsCtx =
            listField "posts" postCtx (return posts) <>
            defaultContext

    getResourceBody
        >>= applyAsTemplate postsCtx
        >>= loadAndApplyTemplate "templates/default.html" postsCtx
        >>= relativizeUrls


myFeedConfiguration :: FeedConfiguration
myFeedConfiguration = FeedConfiguration
    { feedTitle       = "pera's blog"
    , feedDescription = "Just my personal, almost abandoned, weblog..."
    , feedAuthorName  = "Brian Gomes Bascoy"
    , feedAuthorEmail = ""
    , feedRoot        = "http://blog.peramid.es/rss.xml"
    }


myHakyllWriterOptions :: WriterOptions
myHakyllWriterOptions = defaultHakyllWriterOptions
    { writerHTMLMathMethod = MathML }

