---
title: Building for and debugging an ARM Cortex-M
tags:  embedded systems
---

I recently bought an *ST NUCLEO-L432KC*, a small dev board with an [STM32L432KC](https://www.st.com/en/evaluation-tools/nucleo-l432kc.html), a Cortex-M4 micro-controller designed for ultra-low-power applications.[^1] These STM32 MCUs come with a proprietary tool for code generation called STM32CubeMX, unfortunately my experience with it was very poor so to speak: I couldn't even generate a functioning base project for this board.[^2]

Since ST doesn't provide ready to use template projects I had to look for an alternative. [PlatformIO](https://platformio.org/) is a popular tool nowadays for these sorts of things. I've never used it before but I was able to go from zero to blinking LED in less than 5 minutes, literally. My only issue with it was that this felt like cheating, and I was looking to learn a bit more about how these binaries are built, so I decided to build it "from scratch".

First you will need to install a GDB and GCC cross-compiler that can target ARM, the [Newlib](https://sourceware.org/newlib/) C standard library, and [OpenOCD](https://openocd.org/). This will depend on your distro but in Arch Linux you'll find everything in the official repo:

```
sudo pacman -Syu arm-none-eabi-gcc arm-none-eabi-newlib arm-none-eabi-gdb openocd
```

The next step is to obtain [CMSIS](https://arm-software.github.io/CMSIS_5/latest/General/html/index.html) Core (standard APIs for Cortex processors), BSP and HAL drivers for our MCU's family, which is all provided by ST on GitHub:

```
mkdir nucleo-l432kc
cd nucleo-l432kc
git submodule add --depth 1 https://github.com/STMicroelectronics/cmsis-device-l4.git
git submodule add --depth 1 https://github.com/STMicroelectronics/stm32l4xx-hal-driver.git
git submodule add --depth 1 https://github.com/STMicroelectronics/stm32l4xx-nucleo-32-bsp.git
git submodule add --depth 1 https://github.com/STMicroelectronics/cmsis-core.git
```

Now you will need the device [vector table](https://developer.arm.com/documentation/ddi0403/d/System-Level-Architecture/System-Level-Programmers--Model/ARMv7-M-exception-model/The-vector-table) ("*contains the initialization value for the stack pointer, and the entry point addresses of each exception handler*") and the linker script for our MCU. You can find these files and also a bunch of basic examples in the [STM32CubeL4 MCU Firmware Package](https://github.com/STMicroelectronics/STM32CubeL4) repo, which is actually quite heavy, so let's grab just what we need from there:

```
git clone --depth 1 --filter=blob:none --sparse https://github.com/STMicroelectronics/STM32CubeL4.git
cd STM32CubeL4
git sparse-checkout set Projects/NUCLEO-L432KC/Examples/GPIO/GPIO_IOToggle
```

The vector table initialisation code is in `startup_stm32l432kcux.s`, and the linker script for GNU ld is `STM32L432KCUX_FLASH.ld`. This directory includes project files for IDEs we don't care about, but you should copy the example code inside `Src` and `Inc` for blinking the green LED in the board.

For some reason `STM32L432KCUX_FLASH.ld` is missing a symbol named `__end__` that is needed in *newlib*. I haven't look too much into this but maybe ST's fork of newlib does something different. In any case, what I have done is to include the following line in the `._user_heap_stack` almost at the end of the file:

```
. = ALIGN(8);
PROVIDE ( end = . );
PROVIDE ( _end = . );
PROVIDE ( __end__ = . ); /* Add this directive here */
. = . + _Min_Heap_Size;
. = . + _Min_Stack_Size;
. = ALIGN(8);
```

I will save you the pain of having to figure out what exactly you need to compile this simple project with GCC (you can also find all this stuff in my git repo [nucleo-l432kc-template](https://gitlab.com/trobador/nucleo-l432kc-template)):

```makefile
CC = arm-none-eabi-gcc
DEFS = -DSTM32L432xx
CFLAGS = -Wall -Wextra -g -march=armv7e-m+fp -mfloat-abi=hard -mfpu=fpv4-sp-d16 --specs=rdimon.specs
LDFLAGS = -TSTM32L432KCUX_FLASH.ld
CMSIS_DEVICE = cmsis-device-l4/
HAL_DRIVER = stm32l4xx-hal-driver/
NUCLEO_BSP = stm32l4xx-nucleo-32-bsp/
INCLUDES = -Isrc -I$(HAL_DRIVER)/Inc -I$(NUCLEO_BSP) -Icmsis-core/Include -I$(CMSIS_DEVICE)/Include

all:
	$(CC) $(CFLAGS) $(LDFLAGS) $(DEFS) $(INCLUDES) \
	$(HAL_DRIVER)Src/stm32l4xx_hal.c \
	$(HAL_DRIVER)Src/stm32l4xx_hal_cortex.c \
	$(HAL_DRIVER)Src/stm32l4xx_hal_gpio.c \
	$(HAL_DRIVER)Src/stm32l4xx_hal_pwr.c \
	$(HAL_DRIVER)Src/stm32l4xx_hal_pwr_ex.c \
	$(HAL_DRIVER)Src/stm32l4xx_hal_rcc.c \
	$(NUCLEO_BSP)stm32l4xx_nucleo_32.c \
	$(CMSIS_DEVICE)Source/Templates/system_stm32l4xx.c \
	startup_stm32l432kcux.s \
	src/stm32l4xx_it.c \
	src/main.c
```

The `rdimon` specs file is used for [semihosting](https://developer.arm.com/documentation/dui0375/g/What-is-Semihosting-/What-is-semihosting-), which you will need later for debugging. Once you run `make` you should get a new `a.out` file. You can use the common `file` command or `arm-none-eabi-objdump -f` to see that the ELF is for an ARM architecture.

Let's now flash the MCU with this new file. Plug the dev board to a USB port, then open a new terminal and run:

```
openocd -f interface/stlink.cfg -c "transport select hla_swd" -f target/stm32l4x.cfg
```

You should see OpenOCD detecting a Cortex-M4 processor and informing us that GDB is ready to accept new connections on port 3333. Now in another terminal let's use GDB to flash our device:

```
arm-none-eabi-gdb -q a.out

target extended-remote :3333
monitor program a.out verify
```

A message indicating that the programming has finished and successfully verified should appear on the screen. We won't see any blinking LED yet because the state of the MCU is `halted` (you can see this by running `monitor targets`), so start our program inside GDB do:

```
monitor reset run
```

Now you should see the small green LED blinking. You can exit GDB with `q` and kill OpenOCD with Ctrl-C.

To finish this tutorial, let's try some basic debugging capabilities. Open the `main.c` source and look for the `while` loop where the GPIO port connected to the LED gets toggled. Add a `printf("toggled\n");` anywhere inside the loop and include `stdio.h` at the to of the file. Now recompile and reprogram the MCU with this new binary. You may notice the green LED turns on but doesn't blink, this is because the MCU is halted awaiting for a *host* to connect. In the GDB session do:

```
monitor arm semihosting enable
load
monitor reset run
```

At this point you will see that in the OpenOCD terminal our message gets printed every 100 milliseconds. You can do all sorts of things now in GDB, like seeing the current point of execution with the `l` command, or halting the execution and stepping with `monitor halt` and the `s` command.


[^1]: As low as ~10 μA in run mode, and in the order of tens of nanoamps in standby mode
[^2]: I won't go into details here but for instance it just didn't generate a Makefile for me
